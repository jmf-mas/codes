from distributedBFSLD import LeaderElection
from messageTypes import MessageTypes
import os
os.chdir("network")
from distributedBFS import YouNode
os.chdir("network")
from pruning import PruningNode  
os.chdir("network")
from graphGenerator import graphgenerator as graph



m = MessageTypes
if not os.path.exists("out"):
    os.mkdir("out")
for msg in m:
    if os.path.exists(msg.value):
        os.remove(msg.value)  
    os.mknod(msg.value)

def running_you(G, D=10):
    N= G.number_of_nodes()
    yobjects = [YouNode(i, set(G.neighbors(i)), D) for i in G.nodes()]
    isEnd = False
    while not isEnd:
        for i in range(N):
            yobjects[i].oneHop()
              
        for i in range(N):
            yobjects[i].update()
        end = True
        for i in range(N):
            end = end and yobjects[i].isEnded() 
        isEnd = end
        
    
    yLeader = [LeaderElection(node) for node in yobjects]
    isEnd = False
    
    for i in range(N):
        yLeader[i].firstHop()

    while not isEnd:
              
        for i in range(N):
            yLeader[i].treeParentUpdate()
        for i in range(N):
            yLeader[i].treeChildrenUpdate()
        end = True
        for i in range(N):
            end = end and yLeader[i].isEnded() 
        isEnd = end
        
    for i in range(N):
        print("node = ", yLeader[i]._id, "parent = ", yLeader[i]._treeParent, "children = ", yLeader[i]._treeChildren)
    

def running_pruning(G, D=10):
    N= G.number_of_nodes()
    pobjects = [PruningNode(i, set(G.neighbors(i)), D) for i in G.nodes()]
    
    isEnd = False
    for i in range(N):
        pobjects[i].initialOneHop()
    for i in range(N):
        pobjects[i].initialUpdate()
    
    for i in range(N):
        pobjects[i].firstPruningDetection()
    
    while not isEnd:
    
        end = True
        for i in range(N):
            pobjects[i].nextOneHop()
        for i in range(N):
            pobjects[i].nextUpdate()
        for i in range(N):
            end = end and pobjects[i].isEnded() 
        isEnd = end

    pLeader = [LeaderElection(node) for node in pobjects]
    isEnd = False
    
    for i in range(N):
        pLeader[i].firstHop()

    while not isEnd:
              
        for i in range(N):
            pLeader[i].treeParentUpdate()
        for i in range(N):
            pLeader[i].treeChildrenUpdate()
        end = True
        for i in range(N):
            end = end and pLeader[i].isEnded() 
        isEnd = end
        
    for i in range(N):
        print("node = ", pLeader[i]._id, "parent = ", pLeader[i]._treeParent, "children = ", pLeader[i]._treeChildren)
    
N = 10
radius = 8
width = 20
print("Graph creation...")
G = graph(N, width, radius)
print("Graph creation done")
D =5
print("Method by You")
running_you(G, D)
print("Pruning method")
running_pruning(G, D)

import os
os.chdir("..")
from messaging import Messaging
from messageTypes import MessageTypes

class LeaderElection:
    
    def __init__(self, Node):
        
        self._id = Node._id
        self._Ni = Node._Ni
        self._lCentrality = Node._centrality
        
        self._treeParent = None
        self._treeChildren = set([])
        
        self._leaderID = self._id
        self._distance = 0
        self._wait = set([])
        
        self._finished = False
        
        self._msgType = MessageTypes
        self._messaging = Messaging()
            
    def firstHop(self):
        
        for j in self._Ni:
           
            BFS_GO = self._messaging.deserialize(self._msgType.BFS_GO)
            BFS_GO = self._messaging.addTo(BFS_GO, j, self._id, [self._leaderID, self._lCentrality, self._distance])
            self._messaging.serialize(BFS_GO, self._msgType.BFS_GO)
            self._wait.add(j)
            
        self._finished = self._wait==set([])
    
    def isBigger(self, l, cl):
        
        k = self._leaderID
        ck = self._lCentrality
        
        return (ck<cl) or ((ck==cl) and l<k)
    
    def isEnded(self):
        
        return self._finished

    def treeParentUpdate(self):
        BFS_GO = self._messaging.deserialize(self._msgType.BFS_GO)
        if self._id in BFS_GO:
            while len(BFS_GO[self._id])>=1:
                
                j = list(BFS_GO[self._id].keys())[0]
                (mid, cl, dist) = BFS_GO[self._id][j]
                BFS_GO = self._messaging.deleteFrom(BFS_GO, self._id, j)
                self._messaging.serialize(BFS_GO, self._msgType.BFS_GO)
            
                if  self.isBigger(mid, cl):
                    self._leaderID = mid
                    self._distance = 99999999999999
                    self._treeParent = None
                    self._lCentrality = cl
                    
                if self._leaderID==mid and dist+1<self._distance:
                    BFS_BACK = self._messaging.deserialize(self._msgType.BFS_BACK)
                    if self._treeParent!=None:
                        BFS_BACK = self._messaging.addTo(BFS_BACK, self._treeParent, self._id, [self._leaderID, self._lCentrality, self._distance-1, False])
                    
                    self._treeParent = j
                    self._treeChildren = set([])
                    self._distance = dist + 1
                    self._wait = set([])
                    
                    Ni = set(self._Ni).copy()
                    Ni =  Ni.difference(set([self._treeParent]))
                    for k in Ni:
                        BFS_GO = self._messaging.addTo(BFS_GO, k, self._id, [self._leaderID, self._lCentrality, self._distance])
                        self._wait.add(k)
                            
                    if self._wait ==set([]):
                        BFS_BACK = self._messaging.addTo(BFS_BACK, self._treeParent, self._id, [self._leaderID, self._lCentrality, self._distance-1, True])
                        self._finished = True
                    self._messaging.serialize(BFS_BACK, self._msgType.BFS_BACK)
                elif self._leaderID==mid:
                    BFS_BACK = self._messaging.deserialize(self._msgType.BFS_BACK)
                    BFS_BACK = self._messaging.addTo(BFS_BACK, j, self._id, [mid, cl, dist, False])
                    self._messaging.serialize(BFS_BACK, self._msgType.BFS_BACK)
            self._messaging.serialize(BFS_GO, self._msgType.BFS_GO)
    
    def treeChildrenUpdate(self):
        
        BFS_BACK = self._messaging.deserialize(self._msgType.BFS_BACK)
       
        if self._id in BFS_BACK:
            while len(BFS_BACK[self._id])>=1:
                
                j = list(BFS_BACK[self._id].keys())[0]
                (mid, cl, dist, isChild)= BFS_BACK[self._id][j]
                BFS_BACK = self._messaging.deleteFrom(BFS_BACK, self._id, j)

                if self._leaderID==mid and self._distance==dist and not self._finished:
                    self._wait = self._wait.difference(set([j]))
                    if isChild:
                        self._treeChildren.add(j)
                    else:
                        self._treeChildren = self._treeChildren.difference(set([j]))
                    if self._wait==set():
                        if self._treeParent==None:
                            self._finished = True
                        else:                 
                            BFS_BACK = self._messaging.addTo(BFS_BACK, self._treeParent, self._id, [self._leaderID, self._lCentrality, self._distance-1, True])
                            self._finished=True
            self._messaging.serialize(BFS_BACK, self._msgType.BFS_BACK)




            
    
        
        
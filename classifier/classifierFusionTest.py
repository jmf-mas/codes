import numpy as np
from sklearn.metrics import accuracy_score as acc
from sklearn.metrics.classification import log_loss as los
from classifierFusion import majority_vote_rule, sum_rule, product_rule, borda_count_rule
from yayambo import Yayambo
Y = Yayambo(2)
M=200 # number of instances of class 1
N = 200 # number of instances of class 2
m = 5
l = 2
ys = [0]*N
ys = ys + [1]*M
f_1 =[[0.51, 0.49]]*N
f_1 = f_1 +[[0.49, 0.51]]*M
f_1 = np.array(f_1)

f_2 =[[0.9, 0.1]]*N
f_2 = f_2 + [[0.1, 0.9]]*M
f_2 = np.array(f_2)

f_3 =[[0.49, 0.51]]*N
f_3 = f_3 + [[0.51, 0.49]]*M
f_3 = np.array(f_3)

N_ = int(0.65*N)
M_ = int(0.65*M)
f_4 =[[0.7, 0.3]]*(N_)
f_4 = f_4 + np.random.dirichlet(alpha=[1]*l, size =N-N_).tolist()
f_4 = f_4 + [[0.3, 0.7]]*(M_)
f_4 = f_4 + np.random.dirichlet(alpha=[1]*l, size =M-M_).tolist()
f_4 = np.array(f_4)

f_5 =np.random.dirichlet(alpha=[1]*l, size =N+M)

all_classifiers = np.array([f_1, f_2, f_3, f_4, f_5])

y_pred = []
for i in range(m):
    y_pred.append(np.argmax(all_classifiers[i], axis = 1))

m_decisions = []
m_outputs = []

p_decisions = []
p_outputs = []

s_decisions = []
s_outputs = []

b_decisions = []
b_outputs = []

y_decisions = []
y_outputs = []

for i in range(N+M):
    D = []
    for j in range(m):
        D.append(all_classifiers[j, i])
    D=np.array(D)
    m_dist, m_dec = majority_vote_rule(D)
    m_outputs.append(m_dist)
    m_decisions.append(m_dec)
    
    b_dist, b_dec = borda_count_rule(D)
    b_outputs.append(b_dist)
    b_decisions.append(b_dec)
    
    p_dist, p_dec = product_rule(D)
    p_outputs.append(p_dist)
    p_decisions.append(p_dec)
    
    s_dist, s_dec = sum_rule(D)
    s_outputs.append(s_dist)
    s_decisions.append(s_dec)
    
    y_dist, y_dec = Y.getConsencus(D)
    y_outputs.append(y_dist)
    y_decisions.append(y_dec)
    
print("Classifiers accuracy values")
print("-----------------------------------------------")
print("accuracy f_1 = ", acc(ys, y_pred[0]))
print("accuracy f_2 = ", acc(ys, y_pred[1]))
print("accuracy f_3 = ", acc(ys, y_pred[2]))
print("accuracy f_4 = ", acc(ys, y_pred[3]))
print("accuracy f_5 = ", acc(ys, y_pred[4]))
print("-----------------------------------------------")
print("accuracy borda = ", acc(ys, b_decisions))
print("accuracy majority = ", acc(ys, m_decisions))
print("accuracy product = ", acc(ys, p_decisions))
print("accuracy sum = ", acc(ys, s_decisions))
print("accuracy yayambo = ", acc(ys, y_decisions))
print("-----------------------------------------------")
print("Classifiers loss values")
print("-----------------------------------------------")
print("loss f_1 = ", los(ys, f_1))
print("loss f_2 = ", los(ys, f_2))
print("loss f_3 = ", los(ys, f_3))
print("loss f_4 = ", los(ys, f_4))
print("loss f_5 = ", los(ys, f_5))
print("-----------------------------------------------")
print("loss borda = ", los(ys, b_outputs))
print("loss majority = ", los(ys, m_outputs))
print("loss product = ", los(ys, p_outputs))
print("loss sum = ", los(ys, s_outputs))
print("loss yayambo = ", los(ys, y_outputs))
print("-----------------------------------------------")
import numpy as np

def product_rule(D):
    rule = np.prod(D, axis=0)
    distribution = rule/np.sum(rule)
    decision = np.argmax(rule)
    return distribution, decision

def sum_rule(D):
    rule = np.sum(D, axis=0)
    distribution = rule/np.sum(rule)
    decision = np.argmax(rule)
    return distribution, decision

def majority_vote_rule(D):
    Dcopy = [vote(d) for d in D]
    rule = np.sum(Dcopy, axis=0)
    distribution = rule/np.sum(rule)
    decision = np.argmax(rule)
    return distribution, decision

def vote(d):
    k = np.max(d)
    l = len(d)
    delta = [0]*l
    for i in range(l):
        if d[i]==k:
            delta[i]=1
    return delta

def borda_count_rule(D):
    Dcopy=np.copy(D)
    m = len(Dcopy)
    for i in range(m):
        Dcopy[i]=ranking(Dcopy[i]).tolist()
    rule = np.sum(Dcopy, axis=0)
    distribution = rule/np.sum(rule)
    decision = np.argmax(rule)
    return distribution, decision

def ranking(d):
    dcopy = np.copy(d)
    l = len(d)
    for i in range(l):
        j = np.argmax(d)
        dcopy[j] = l-i
        d[j]=-1
    return dcopy

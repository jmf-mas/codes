import numpy as np
from classifierFusion import sum_rule

class Yayambo:
    
    def __init__(self, m, T=50, epsilon = 1.0e-06, epsilon0 = 0.000001):
        
        self.__T = T
        self.__epsilon = epsilon
        self.__epsilon0 = epsilon0
        self.__m = m
    
    def support(self, x, y):
        
        dis = []
        for xi, yi in zip(x, y):
            dis.append(np.abs(xi*np.log((self.__epsilon0+xi)/(self.__epsilon0+yi))))
        return y/(1.+np.array(dis))

    def convergence(self, OD, LD):
        
        diff = np.linalg.norm(OD-LD, axis=1)
        diff = np.sum(diff)
        return diff < self.__m*self.__epsilon
    
    def update(self, pi, betas):
        
        S = pi*np.sum(betas, axis=0)
        S /=np.sum(S)
        return S.tolist()
    
    def getConsencus(self, D):
        
        m, l = np.array(D).shape
        Dcopy = np.copy(D)
        
        for t in range(self.__T):
            tempD = []
            
            for i in range(m):
                betas = []
                
                for j in range(m):
                    
                    if j!=i:
                        bji=self.support(Dcopy[i], Dcopy[j])
                        betas.append(bji)

                tempD.append(self.update(Dcopy[i], betas))
                
            if self.convergence(Dcopy, tempD):
                distribution, decision = sum_rule(Dcopy)
                return distribution, decision
            Dcopy = np.copy(tempD)
        distribution, decision = sum_rule(Dcopy)
        
        return distribution, decision

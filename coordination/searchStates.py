import enum

class SearchStates(enum.Enum):
    
    INREGION = "Inside its exploration region"
    OUTREGION = "Outside its exploration region"

import enum
class PlanningStates(enum.Enum):
    
    CONSTRUCTION = "View construction"
    ELECTION = "Leader election"
    FUSION = "Data fusion"
    TASK = "Task assignment"
    NONE = "None"
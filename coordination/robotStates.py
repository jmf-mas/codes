import enum

class RobotStates(enum.Enum):
    
    PLANNING = "Planning"
    COMPLETE = "Completed"
    INTERACTING = "Interacting"
    WAITING = "Waiting for a planning robot to finish"
    STOP = "Stopped"
    SEARCHING = "Searching"
    INSPECTION = "Waiting for potential  robots in its area to be within perception region"
    # for PRS
    RENDEZVOUS = "Moving to rendezvous"
    

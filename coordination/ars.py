import numpy as np
from coordinationUtils import Ball
from robotStates import RobotStates
from planningStates import PlanningStates
from bug import Bug
import os
os.chdir("../network")
from pruning import PruningNode
os.chdir("leader")
from distributedBFSLD import LeaderElection
from scipy.optimize import linear_sum_assignment as hungarian
from messaging import Messaging
from messageTypes import MessageTypes

class ARS:
    
    def __init__(self, i, tau, d, env, DeltaTij=10, N=360, eps=1, h=100, gamma=1, deltaT=1, eta=1.5, Tw=3):
        
        self._id = i
        # related objects
        self._Bug = Bug(d, gamma)
        self._pruningObject = None
        self._electionObject = None
        
        # param
        self._DeltaTij = DeltaTij
        self._N = N
        self._eps = eps
        self._gamma = gamma
        self._h = h
        self._deltaT = deltaT
        self._eta = eta
        self._Tw = Tw
        self._tw = 0
        
        # initial
        self._O, self._xi = env
        self._y0 = self._xi.copy()
        self._Deltay = 0
        self._d = d
        self._t = 0
        self._tau = tau
        self._robotState = RobotStates.SEARCHING
        self._planningState = PlanningStates.NONE
        
        self._msgType = MessageTypes
        self._messaging = Messaging()
        
        self.reinitInteractionInfo()
        
        # data
        alpha = np.random.uniform(0, 2*np.pi)
        self._ai = self._xi + self._d*np.array([np.cos(alpha), np.sin(alpha)])
        B = Ball(self._xi, self._d)
        self._Cit = [B.Ball]
        self._Gi = {}
        self._Jit = []
        self._Ti = 10
        self._A  = set([])
        self._gMembers = None

    
    def reinitInteractionInfo(self):
        
        self._treeParent = None
        self._treeChildren = set([])
        self._Ii = set([])
        self._Wi = set([])
        self._Li = {}
        self._Gamma = {}
        self._D = set([])
        self._Dj = {}
        self._planningState = PlanningStates.NONE
        self._tw = 0
        self._DeltaTi = 0
        self._robotState = RobotStates.SEARCHING
    
    def isEnded(self):
        end = self._t>=self._tau
        if end:
            self._robotState = RobotStates.COMPLETE
        return end
        
    def update(self, meetings):
        
        if not self.isEnded():
            if self._robotState in [RobotStates.SEARCHING,  RobotStates.WAITING]:
                
                Ii, Wi = self.newInteraction(meetings) 
                if len(Ii)>=1:
                    self._Ii, self._Wi = Ii.copy(), Wi.copy()
                    self._robotState = RobotStates.STOP
                    self._tw = self._t
                    
                elif len(Wi)>=1:
                    self._robotState = RobotStates.WAITING
                else:
                    
                    self.search()
                        
            elif self._robotState == RobotStates.STOP:
                
                if (self._t-self._tw)<=self._Tw:
                    self.waiting(meetings)
                else:
                    self._robotState = RobotStates.PLANNING
                
            elif self._robotState == RobotStates.PLANNING:
                
                if self._planningState ==PlanningStates.NONE:
                    
                    self._pruningObject = PruningNode(self._id, self._Ii)
                    self._pruningObject.initialOneHop()
                    self._planningState = PlanningStates.CONSTRUCTION
                    self._stepB = "update"
                    self._GO = False
                    
                elif self._planningState==PlanningStates.CONSTRUCTION:
                    self.treeConstruction()
                    
                elif self._planningState==PlanningStates.ELECTION:
                    
                    self.leaderElection()
                    
                elif self._planningState==PlanningStates.FUSION:
                    self.dataFusion()
                
                elif self._planningState == PlanningStates.TASK:
                    self.taskAssignment()
                    
        B = Ball(self._xi, self._d)
        self._Cit.append(self._Cit[-1].union(B.Ball))
        self._t +=self._deltaT
        self._DeltaTi +=self._deltaT
    
    def search(self):
        
        xi = self._Bug.distanceBug(self._A, self._xi, self._ai, self._Cit[-1], self._O)
        self._xi = xi
        self._Deltay +=self._gamma
        
        if self._Deltay>self._eta*np.linalg.norm(self._ai-self._y0):
            self._ai = self.getReleaseTarget(self._N)
            self._y0 = self._xi.copy()
            self._Deltay = 0
            
        elif np.linalg.norm(self._ai-self._xi)<self._eps:
            self._ai = self.getNextTarget(self._N)
            self._y0 = self._xi.copy()
            self._Deltay = 0
            
    def treeConstruction(self):
        
        if not self._pruningObject.isEnded():
            if self._stepB == "update":
                self._pruningObject.initialUpdate()
                self._pruningObject.firstPruningDetection()
                self._stepB = "nexthop"
            elif self._stepB == "nexthop":
                self._pruningObject.nextOneHop()
                self._stepB = "nextupdate"
            else:
                self._pruningObject.nextUpdate()
                self._stepB = "nexthop"
        else:
            self._planningState=PlanningStates.ELECTION
            self._leaderObject = LeaderElection(self._pruningObject)
            self._leaderObject.firstHop()
            self._GO = True
        
    def leaderElection(self):

        if self._GO:
            self._leaderObject.treeParentUpdate()
            self._GO = False
        else:
            self._leaderObject.treeChildrenUpdate()
            self._GO = True
        if self._leaderObject.isEnded():
            BFS_GO = self._messaging.deserialize(self._msgType.BFS_GO)
            BFS_BACK = self._messaging.deserialize(self._msgType.BFS_BACK)
            endGO = self._id not in BFS_GO  or  (self._id in BFS_GO and len(BFS_GO[self._id])==0)
            endBACK =  self._id not in BFS_BACK  or  (self._id in BFS_BACK and len(BFS_BACK[self._id])==0)
            if endGO and endBACK:
                self._treeChildren = self._leaderObject._treeChildren
                self._treeParent = self._leaderObject._treeParent
                self._planningState=PlanningStates.FUSION  
                    
    def getNextTarget(self, N):
        
        alpha = np.random.uniform(0, 2*np.pi)
        a = self._xi + (self._d+self._gamma)*np.array([np.cos(alpha), np.sin(alpha)])
        ball = Ball(a, self._d+self._gamma)
        (u, beta) = (len(ball.Ball.intersection(self._Cit[-1])), np.pi/N)
        for j in range(N):
            alpha = alpha + beta
            b = self._xi + (self._d+self._gamma)*np.array([np.cos(alpha), np.sin(alpha)])
            ball = Ball(b, self._d+self._gamma)
            if len(ball.Ball.intersection(self._Cit[-1]))<u:
                a = b.copy()
                u = len(ball.Ball.intersection(self._Cit[-1]))
        return a
    
    def getReleaseTarget(self, N):
        
        alpha = np.random.uniform(0, 2*np.pi)
        a = self._xi + 2*self._d*np.array([np.cos(alpha), np.sin(alpha)])
        ball = Ball(a, 2*self._d)
        (u, beta) = (len(ball.Ball.intersection(self._Cit[-1])), np.pi/N)
        for j in range(N):
            alpha = alpha + beta
            b = self._xi + self._d*np.array([np.cos(alpha), np.sin(alpha)])
            ball = Ball(b, 2*self._d)
            if len(ball.Ball.intersection(self._Cit[-1]))<u:
                a = b.copy()
                u = len(ball.Ball.intersection(self._Cit[-1]))
        return a
    
    def getCentralPoint(self):
        
        P = len(self._Li)
        c = np.array([0, 0])
        for j in self._Li:
            c = c + self._Li[j]
        return c/P
    
    def getCoordinationTargets(self):
        
        P = len(self._Li)
        c = self.getCentralPoint()
        alpha = np.random.uniform(0, 2*np.pi)
        beta = 2*np.pi/P
        B = []
        for j in range(P):
            bj = c + self._h*np.array([np.cos(alpha), np.sin(alpha)])
            alpha = alpha + beta
            B.append(bj)
             
        robotsIDS, cost = self.getCostMatrix(B)
        ids, targets = hungarian(cost)
        self._Gamma = {robotsIDS[i]:B[a] for (i, a) in zip(ids, targets)}
    
    def timeOfPreviousMeeting(self, j):
        
        tm = -1
        if j in self._Gi:
            tm = self._Gi[j]
        return tm
    
    def getCostMatrix(self, B):
        
        cost = []
        ids  = []
        for i in self._Li:
            ids.append(i)
            row = []
            for a in B:
                row.append(np.linalg.norm(np.array(self._Li[i])-a))
            cost.append(row)
        return ids, cost
        
    def newInteraction(self, meetings):

        withPerceptionRange = meetings
        Ii = set([])
        Wi = set([])
        for (j, state, tw) in withPerceptionRange:
            tm = self.timeOfPreviousMeeting(j)
            if (tm==-1 or self._DeltaTi>self._Ti) and (state==RobotStates.SEARCHING or state == RobotStates.STOP):
                Ii.add(j)
                self._robotState = RobotStates.STOP
            elif state == RobotStates.PLANNING and j not in self._Ii:
                Wi.add(j)
                self._robotState = RobotStates.STOP
                
        return Ii, Wi
    
    def relayInformation(self):
        
        F = self._treeChildren.copy()
        TASK = self._messaging.deserialize(self._msgType.TASK)
        while F!=set([]):
            j = F.pop()
            Gamma = {}
            
            for k in self._Dij[j]:
                Gamma[k] = self._Gamma[k]
            
            TASK = self._messaging.addTo(TASK, j, self._id, [self._Cit[-1], self._Gi, self._Jit[-1], Gamma])
            
        self._messaging.serialize(TASK, self._msgType.TASK)
        
            
    def taskAssignment(self):
        
        if self._treeParent == None:
            self.getCoordinationTargets()
            for j in self._treeChildren:
                for k in self._Gamma:
                    self._Gi[k] = self._t
                    
            self.relayInformation()
            
            self._y0 = self._xi.copy()
            self._Deltay = 0 
                
            if type(self).__name__=="ARS":
                self._ai = self._Gamma[self._id]
            else:
                self._ai, self._rTime, self._rPoint, self._gMembers = self._Gamma[self._id]
                self._gMembers = self._gMembers.difference(set([self._id]))

            self.reinitInteractionInfo()
                    
        else:
            TASK = self._messaging.deserialize(self._msgType.TASK)
            if self._id in TASK:
                
                if(len(TASK[self._id])==1):
                    
                    j = list(TASK[self._id].keys())[0]
                    [Cj, Gj, Jj, Gammaj]= TASK[self._id][j].copy()
                        
                    self._Cit.append(self._Cit[-1].union(Cj))
                    self._Gi.update(Gj)
                    if len(self._Jit)==0:
                        self._Jit.append(Jj)
                    else:
                        self._Jit[-1] = self._Jit[-1].union(Jj)
                    self._Gamma.update(Gammaj)
                        
                    TASK = self._messaging.deleteFrom(TASK, self._id, j)
 
                self._messaging.serialize(TASK, self._msgType.TASK)
                self.relayInformation()

                self._robotState = RobotStates.SEARCHING
                if self._id in self._Gamma:
                    self._y0 = self._xi
                    self._Deltay = 0 
                    if type(self).__name__=="ARS":
                            
                        self._ai = self._Gamma[self._id]
                    else:
                        self._ai, self._rTime, self._rPoint, self._gMembers = self._Gamma[self._id]
                        self._gMembers = self._gMembers.difference(set([self._id]))

                self.reinitInteractionInfo()
                
    def dataFusion(self):
        
        F = self._treeChildren.copy()
        self._Li[self._id] = self._xi.copy()
        self._Di = set([])
        self._Dij = {}
        
        if len(F)==0:
            DATA = self._messaging.deserialize(self._msgType.DATA)
            j = self._treeParent
            
            DATA = self._messaging.addTo(DATA, j, self._id, [self._Cit[-1].copy(), self._Ii.union(set([self._id])), self._Li.copy(), self._Di.copy()])
            
            self._messaging.serialize(DATA, self._msgType.DATA)
            self._planningState = PlanningStates.TASK
            
        else:
            DATA = self._messaging.deserialize(self._msgType.DATA)
            if self._id in DATA:
                self._sendingInformation = True
                if len(F)==len(DATA[self._id]):
                    while(len(DATA[self._id])>=1):
                        j = list(DATA[self._id].keys())[0]
                        self._Di.add(j)
                        
                        [Cj, Ij, Lj, Dj]= DATA[self._id][j]
                        self._Cit[-1] = self._Cit[-1].union(Cj)
                        self._Ii = self._Ii.union(Ij)
                        self._Li.update(Lj)
                        if len(self._Jit)==0:
                            self._Jit.append(self._Ii)
                        else:
                            self._Jit.append(self._Jit[-1].union(self._Ii))
                        if j in self._Dij:
                            self._Dij[j] = self._Dij[j].union(Dj).union(set([j]))
                        else:
                            self._Dij[j] = Dj.union(set([j]))
                        self._Di = self._Di.union(Dj)
                        self._Dij[j].add(j)
                            
                        self._messaging.deleteFrom(DATA, self._id, j)
                        
                    self._messaging.serialize(DATA, self._msgType.DATA)
                    
                    DATA = self._messaging.deserialize(self._msgType.DATA)
                    for j in F:
                        DATA = self._messaging.addTo(DATA, j, self._id, [self._Cit[-1].copy(), self._Ii.union(set([self._id])), self._Li.copy(), self._Di.copy()])
                        
                    if self._treeParent==None:
                        self._messaging.serialize(DATA, self._msgType.DATA)
                        self._planningState = PlanningStates.TASK
           
    def waiting(self, meetings):
        
        withPerceptionRange = meetings
        for (j, state, tw) in withPerceptionRange: 
            if (state == RobotStates.SEARCHING or state == RobotStates.STOP) and j not in self._Ii:
                self._tw = max(tw, self._tw)
                self._Ii.add(j)
            
            
    
    
            
            
                
        
        
            
        
        
            
            
    
    
                
        
    
    
        
    
    
                
                
            
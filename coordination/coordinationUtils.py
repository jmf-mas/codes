import numpy as np
class Rectangle:
    
    def __init__(self, xy, w, h):
        
        self.x = xy[0]
        self.y = xy[1]
        self.w = w
        self.h = h
    
    def isPointInside(x, params):
        lower, width, height = params
        return x[0]>=lower[0] and x[0]<=lower[0]+width and x[1]>=lower[1] and x[1]<=lower[1]+height

class Environment:
    
    def __init__(self, d, width, height, w, h, N):
        
        self.__W = Rectangle([0, 0], width, height)
        generator = Generator()
        self.__O = generator.getObstacles(d, width, height, w, h)
        self.__positions = generator.getRobotInitialPositions(d, N, width, height, self.__O)
        
    def getEnv(self, i):
        
        return self.__O, self.__positions[i]
        

class Ball:
    
    def __init__(self,xy, d):
        x, y = int(xy[0]), int(xy[1])
        self.Ball = set([(i, j) for i in range(x-d, x+d) for j in range(y-d, y+d) if (x-i)**2+(y-j)**2<=d**2])
        

class Generator:
    
    def getObstacles(self, d, xmax, ymax, w, h, low=5, high=30):
        
        obstacles=set([])
        N = np.random.randint(low, high)
        for n in range(N):
            itype = np.random.choice(["rectangle", "circle"])
            x, y = np.random.randint(xmax), np.random.randint(ymax)
            width, height = np.random.randint(w), np.random.randint(h)
            if itype=="rectangle":
                obstacles = obstacles.union(set([(i, j) for i in range(x, x+width) for j in range(y, x+height)]))
            else:
                r  = int(width/2)
                obstacles = obstacles.union(set([(i, j) for i in range(x-r, x+r) for j in range(y-r, y+r) if (x-i)**2+(y-j)**2<=r**2]))
                
        # boundary
        obstacles = obstacles.union(set([(i, j) for i in range(-d, 4) for j in range(-d, ymax)]))
        obstacles = obstacles.union(set([(i, j) for i in range(xmax-4, xmax) for j in range(-d, ymax)]))
        obstacles = obstacles.union(set([(i, j) for i in range(-d, xmax) for j in range(-d, 4)]))
        obstacles = obstacles.union(set([(i, j) for i in range(-d, xmax) for j in range(ymax-4, ymax)]))
        
        return obstacles
    
    
    def getRobotInitialPositions(self, d, N, w, h, obstacles):
        
        positions={}
        n=0
        while n < N:
            pos = [np.random.randint(w), np.random.randint(h)]
            xin = 2*d<=pos[0]<=w-2*d
            yin = 2*d<=pos[1]<=h-2*d
            if tuple(pos) not in obstacles and xin and yin:
                positions[n] = np.array(pos)
                n +=1
        
        return positions
    
class Meeting:  
    
    def getMeetings(self, i, robots, d):
        meetings = []
        ri = robots[i]
        for rj in robots:
            if ri._id!=rj._id and np.linalg.norm(ri._xi-rj._xi)<=d:
                meetings.append((rj._id, rj._robotState, rj._tw))
        return meetings
    


    

        
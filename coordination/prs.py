import numpy as np
from ars import ARS
import os
from coordinationUtils import Ball
from robotStates import RobotStates
from planningStates import PlanningStates

os.chdir("network")
from pruning import PruningNode
from scipy.optimize import linear_sum_assignment as assignment
os.chdir("..")

class PRS(ARS):
    
    def __init__(self, i, tau, d, env, a=40, DeltaTij=10, N=360, eps=1, h=100, gamma=1, deltaT=1, eta=2, Tw=3):
        
        super(PRS, self).__init__(i, tau, d, env, DeltaTij, N, eps, h, gamma, deltaT, eta, Tw)
        
        self._rTime = None
        self._rPoint = None
        self._a = a
        self._b = self._a/10
        
        self._gMembers = set([])
        
    
    def getCoordinationTargets(self):
        
        P = len(self._Li)
        c = self.getCentralPoint()
        alpha = np.random.uniform(0, 2*np.pi)
        beta = 2*np.pi/P
        B = []
        for j in range(P):
            bj = c + self._h*np.array([np.cos(alpha), np.sin(alpha)])
            alpha = alpha + beta
            B.append(bj)
             
        self._rTime = self._t + 2*self._a + self._b
        i = np.random.randint(0, len(self._Cit[-1]))
        self._rPoint = list(self._Cit[-1])[i]
        self._gMembers = self._Ii.copy()
        
        robotsIDS, cost = self.getCostMatrix(B)
        ids, targets = assignment(cost)

        self._Gamma = {robotsIDS[i]:[B[a], self._rTime, self._rPoint, self._gMembers] for (i, a) in zip(ids, targets)}
    
    def reinitInteractionInfo(self):
        
        self._treeParent = None
        self._treeChildren = set([])
        self._Ii = set([])
        self._Wi = set([])
        self._Li = {}
        self._Gamma = {}
        self._D = set([])
        self._Dj = {}
        self._planningState = PlanningStates.NONE
        self._DeltaTi = 0
        self._robotState = RobotStates.SEARCHING
        self._tw = 0
            
    def update(self, meetings):
        
        if self._gMembers == set([]):
            
            if self._robotState in [RobotStates.SEARCHING,  RobotStates.WAITING]:
                
                Ii, Wi = self.newInteraction(meetings) 
                if len(Ii)>=1:
                    self._Ii, self._Wi = Ii.copy(), Wi.copy()
                    self._robotState = RobotStates.INSPECTION
                    self._tw = self._t
                elif len(Wi)>=1:
                    self._robotState = RobotStates.WAITING
                else:
                    xi = self._Bug.distanceBug(self._A, self._xi, self._ai, self._Cit[-1], self._O)
                    self._xi = np.array(xi)
                    self._Deltay +=self._gamma
                        
                    if self._Deltay>self._eta*np.linalg.norm(self._ai-self._y0):
                        self._ai = self.getReleaseTarget(self._N)
                        self._y0 = self._xi.copy()
                        self._Deltay = 0
                        
                    elif np.linalg.norm(self._ai-self._xi)<self._eps:
                        self._ai = self.getNextTarget(self._N)
                        self._y0 = self._xi.copy()
                        self._Deltay = 0
                        
            elif self._robotState ==RobotStates.INSPECTION:
                if (self._t-self._tw)<=self._Tw:
                    self.waiting(meetings)
                else:
                    self._robotState = RobotStates.PLANNING
                    
            elif self._robotState == RobotStates.PLANNING:
                self.interaction()
                    
        else:
            if self._robotState == RobotStates.INSPECTION:
                Ii = set([])
                for (j, _, _) in meetings:
                    Ii.add(j)
                if (self._t-self._tw)<=self._Tw:
                    if Ii.intersection(self._gMembers)!= set([]):
                        self._robotState = RobotStates.PLANNING
                        self._Ii = Ii.copy()
                    else:
                        self.waiting(meetings)
                else:
                    
                    if Ii.intersection(self._gMembers)!= set([]):
                        self._robotState = RobotStates.PLANNING
                        self._Ii = Ii.copy()
                    else:
                        self._gMembers = set([])
                        self._robotState = RobotStates.SEARCHINGG
                        self._ai = self.getReleaseTarget(self._N)
                        self._y0 = self._xi.copy()
                        self._Deltay = 0
                        
            elif self._robotState == RobotStates.SEARCHING:
                
                if self._t>=self._rTime-(self._a + self._b):
                    self._robotState = RobotStates.RENDEZVOUS                       
                else:
                    xi = self._Bug.distanceBug(self._A, self._xi, self._ai, self._Cit[-1], self._O)
                    self._xi = np.array(xi)
                    self._Deltay +=self._gamma
                        
                    if self._Deltay>self._eta*np.linalg.norm(self._ai-self._y0):
                        self._ai = self.getReleaseTarget(self._N)
                        self._y0 = self._xi.copy()
                        self._Deltay = 0
                        
                    elif np.linalg.norm(self._ai-self._xi)<self._eps:
                        self._ai = self.getNextTarget(self._N)
                        self._y0 = self._xi.copy()
                        self._Deltay = 0
                         
            elif self._robotState == RobotStates.PLANNING:
                self.interaction()
                
            elif self._robotState == RobotStates.RENDEZVOUS:
                if np.linalg.norm(np.array(self._rPoint) - self._xi)<=self._eps:
                    self._state = RobotStates.INSPECTION
                    self._tw= self._t
                else:
                    xi = self._Bug.distanceBug(self._A, self._xi, self._rPoint, self._Cit[-1], self._O)
                    self._xi = np.array(xi)
                    self._Deltay +=self._gamma
                    
            
        ball = Ball(self._xi, self._d)
        self._Cit.append(self._Cit[-1].union(ball.Ball))
        self._t +=self._deltaT
        self._DeltaTi +=self._deltaT
        
    def interaction(self):
        
        if self._planningState ==PlanningStates.NONE:
            self._pruningObject = PruningNode(self._id, self._Ii)
            self._pruningObject.initialOneHop()
            self._planningState = PlanningStates.CONSTRUCTION
            self._stepB = "update"
            self._GO = False
            
        elif self._planningState==PlanningStates.CONSTRUCTION:
            self.treeConstruction()
            
        elif self._planningState==PlanningStates.ELECTION:
            
            self.leaderElection()
            
        elif self._planningState==PlanningStates.FUSION:
            self.dataFusion()
        
        elif self._planningState == PlanningStates.TASK:
            self.taskAssignment() 
    
        
            
            

        
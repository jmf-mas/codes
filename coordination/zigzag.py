import numpy as np
from coordinationUtils import Ball

class Zigzag:
    
    def __init__(self, gamma, d):
        
        self._d = d
        self._gamma = gamma
           
    def paramReinit(self, Xi, xi, ci):
        
        self._Xi = Xi
        self._xi = xi
        self._ci = ci
        self._dist = 0
        self._J = 1
        self.setPattern()
        self.setDirection()
        self.setDistance()
             
    def getPatterns(self):
        
        return [[self.East(), self.North(), self.West(), self.North()], 
            [self.East(), self.South(), self.West(), self.South()], 
            [self.West(), self.North(), self.East(), self.North()],
            [self.West(), self.South(), self.East(), self.South()], 
            [self.North(), self.East(),self.South(), self.East()], 
            [self.North(), self.West(), self.South(), self.West()],
            [self.South(), self.East(),self.North(), self.East()], 
            [self.South(), self.West(), self.North(), self.West()]]
        
    def North(self):
        
        return [0, self._gamma]
    
    def South(self):
        
        return [0, -self._gamma]
    
    def East(self):
                    
        return [self._gamma, 0]
    
    def West(self):
        
        return [-self._gamma, 0]
    
    def setPattern(self):
        
        x, y, w, h = int(self._Xi.x), int(self._Xi.y), int(self._Xi.w), int(self._Xi.h)
        if x == self._ci[0] and y == self._ci[1]:
            self._pattern =  self.getPatterns()[np.random.choice([0, 4])]
        elif x == self._ci[0] and y + h == self._ci[1]:
            self._pattern =  self.getPatterns()[np.random.choice([1, 6])]
        elif x + w == self._ci[0] and y == self._ci[1]:
            self._pattern =  self.getPatterns()[np.random.choice([2, 5])]
        else:
            self._pattern =  self.getPatterns()[np.random.choice([3, 7])]
    
    def setDirection(self):
        
        self._direction = self._pattern[int(self._J%4)-1]
    
    def getStartingPoint(self):
        
        if (self._pattern[0]==self.North() and self._pattern[1]==self.East()) or (self._pattern[0]==self.East() and self._pattern[1]==self.North()):
            return np.array(self._ci) + [self._d, self._d]
        elif (self._pattern[0]==self.South() and self._pattern[1]==self.East()) or (self._pattern[0]==self.East() and self._pattern[1]==self.South()):
            return  np.array(self._ci) + [self._d, -self._d]
        elif (self._pattern[0]==self.North() and self._pattern[1]==self.West()) or (self._pattern[0]==self.West() and self._pattern[1]==self.North()):
            return  np.array(self._ci) + [-self._d, self._d]
        else:
            return  np.array(self._ci) + [-self._d, -self._d]
        
    def setDistance(self):
        
        if self._J%2==1:
            if np.abs(self._direction).tolist()==[0, self._gamma]:
                self._distance =  int(self._Xi.h-2*self._d)
            else:
                self._distance = int(self._Xi.w-2*self._d)
        else:
            if np.abs(self._direction).tolist()==[0, self._gamma]:
                self._distance = int(np.min([2*self._d, self._Xi.h-self._d*self._J]))
            else:
                self._distance = int(np.min([2*self._d, self._Xi.w-self._d*self._J]))
            
    def zigzagMove(self, O):
        
        self._B = set([])
        xi = self._xi + self._direction
        obstacle = self.obstacleDetection(xi, O)
        if not obstacle and self._dist<=self._distance-self._gamma and self.isInside(xi):
            self._xi = xi
            self._dist +=self._gamma
        else:
            
            if obstacle:
                j = 1
                for d in range(self._dist, self._distance):
                    ball = Ball(self._xi + j*np.array(self._direction), self._d)
                    j +=1
                    self._B = self._B.union(ball.Ball)
                
            self._J = (self._J+1)%4
            self._direction = self._pattern[self._J-1]
            xi = self._xi + self._direction
            self._dist =self._gamma
            self.setDistance()
            obstacle = self.obstacleDetection(xi, O)
            if not obstacle and self.isInside(xi):
                self._xi = xi
    
    def obstacleDetection(self, x, O):
        
        x0 = int(x[0]) +self._d*self._direction[0]
        y0 = int(x[1]) +self._d*self._direction[1]
        
        return (x0, y0) in O
    
    def isInside(self, xi):
        
        xin = self._Xi.x + self._d <= xi[0]<=self._Xi.x + self._Xi.w - self._d
        yin = self._Xi.y + self._d <= xi[1]<=self._Xi.y + self._Xi.h - self._d
        
        return xin and yin
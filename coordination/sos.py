import numpy as np
from coordinationUtils import Ball, Rectangle
from zigzag import Zigzag
from robotStates import RobotStates
from planningStates import PlanningStates
from bug import Bug
from searchStates import SearchStates
import os
os.chdir("../network")
from pruning import PruningNode
os.chdir("leader")
from distributedBFSLD import LeaderElection
from scipy.optimize import linear_sum_assignment as hungarian
from messaging import Messaging
from messageTypes import MessageTypes

class SOS:
    
    def __init__(self, i, tau, d, env, DeltaTij=10, N=360, eps=1, h=100, gamma=1, deltaT=1, eta=3, Tw=3, s=5):
        
        # external params
        self._id = i
        self._tau = tau
        self._d = d
        self._DeltaTij = DeltaTij
        self._N = N
        self._eps = eps
        self._h = h
        self._gamma = gamma
        self._deltaT = deltaT
        self._eta = eta
        self._Tw = Tw
        self._tw = 0
        self._s = s
        self._As = 2*s*self._gamma*self._d/3
        
        # internal params
        self._O, self._xi = env
        self._y0 = self._xi.copy()
        self._Deltay = 0
        self._t = 0
        self._w = np.sqrt(np.pi*self._d**2 + 2*self._gamma*self._tau*self._d)+1
        self._A = 2*self._d*np.sqrt(np.pi*self._d**2 + 2*self._gamma*self._tau*self._d)
        self._m = 2*self._d
        self._L=10
        self._w0 = 4*self._d
        self._AB= set([])
        
        # SOS related objects
        self._Yit = []
        self._Zi = set([])
        ball = Ball(self._xi, self._d)
        self._Bit = [ball.Ball]
        self._Cit = [ball.Ball]
        alpha = np.random.uniform(0, 2*np.pi)
        self._ai = self._xi + self._d*np.array([np.cos(alpha), np.sin(alpha)])
        self._Gi = {}
        self._Jit = []
        self._Ti = 10
        self.reinitInteractionInfo()

        # related objects
        self._Bug = Bug(d, gamma)
        self._pruningObject = None
        self._electionObject = None
        self._zigzag = Zigzag(self._gamma, self._d)
        self._msgType = MessageTypes
        self._messaging = Messaging()
        
        # states
        self._robotState = RobotStates.SEARCHING
        self._planningState = PlanningStates.NONE
        self._searchState = SearchStates.INREGION
    
        self.initXi()
    
    def reinitInteractionInfo(self):
        
        self._treeParent = None
        self._treeChildren = set([])
        self._Ii = set([])
        self._Wi = set([])
        self._Li = {}
        self._Gamma = {}
        self._D = set([])
        self._Dj = {}
        self._planningState = None
        self._tw = 0
        self._DeltaTi = 0
        self._AB = set([])

        self._robotState = RobotStates.SEARCHING
        self._planningState = PlanningStates.NONE
        self._searchState = SearchStates.OUTREGION
        
        if len(self._Yit)>0:
            Xi = self.rectangleToSet(self._Xi)
            Yi = set([])
            for yi in self._Yit:
                Yi= Yi.union(self.rectangleToSet(yi))
            Xi  = Xi.difference(self._Bit[-1])
            Xi = Xi.difference(Yi)
            A = 2*self._d*np.sqrt(np.pi*self._d**2 + 2*self._gamma*(self._tau-self._t)*self._d)/2
            
            if len(Xi)>0:
                Q = self.rectangleExtraction(self._N, Xi)
                if Q.w*Q.h>A/2:
                    self._Xi = Q
                else:
                    self._Xi = self._Yit.pop()
            else:
                self._Xi = self._Yit.pop()
            
            self.reinitZigzagParams()
            
            self._y0 = self._xi.copy()
            self._Deltay = 0
            self._AB = set([])
            
    def initXi(self):
        
        X = [Rectangle([self._xi[0]-self._d, self._xi[1]-self._d], self._w, self._w)]
        X.append(Rectangle([self._xi[0]-self._d, self._xi[1]-self._w + self._d], self._w, self._w))
        X.append(Rectangle([self._xi[0] - self._w + self._d, self._xi[1]-self._d], self._w, self._w))
        X.append(Rectangle([self._xi[0] - self._w + self._d, self._xi[1] - self._w + self._d], self._w, self._w))
        i = np.random.randint(0, 4)
        self._Xi = X[i]
        self._Vi = self._Xi
        self._searchState = SearchStates.INREGION
        self.reinitZigzagParams()
        
    def reinitZigzagParams(self):
        
        self._ci = self.getClosestCorner(self._Xi, self._xi)
        self._zigzag.paramReinit(self._Xi, self._xi, self._ci)
        self._ai = self._zigzag.getStartingPoint()
        
    def isEnded(self):
        end = self._t>=self._tau
        if end:
            self._robotState = RobotStates.COMPLETE
        return end
        
    def search(self):
        if self._searchState == SearchStates.INREGION:
            
            if (self._t<self._s or len(self._Cit[-1].difference(self._Cit[-(self._s+1)]))>=self._As):
                self._zigzag.zigzagMove(self._O) 
                self._xi = self._zigzag._xi
                self._Bit[-1] = self._Bit[-1].union(self._zigzag._B)
                self._Deltay = 0
                self._AB = set([])
            else:
                self.finExplorationAreaInRegion()
                self._searchState = SearchStates.OUTREGION
                self.reinitZigzagParams()
                self._y0 = self._xi.copy()
                self._Deltay = 0
                self._AB = set([])
                    
        elif self._searchState == SearchStates.OUTREGION:
            
            xi = self._Bug.distanceBug(self._AB, self._xi, self._ai, self._Cit[-1], self._O)
            self._xi = np.array(xi)
            self._Deltay +=self._gamma
            self.reinitZigzagParams()
            
            if np.linalg.norm(self._xi-self._ai)<self._eps:
                self._searchState = SearchStates.INREGION
                self.reinitZigzagParams()
                self._y0 = self._xi.copy()
                self._Deltay = 0
                self._AB = set([])
                
            elif self._Deltay>self._eta*np.linalg.norm(self._ai-self._y0):
                
                self._Bit[-1] = self._Bit[-1].union(self.rectangleToSet(self._Xi))
                if len(self._Yit)>0:
                    self._Xi = self._Yit.pop()
                else:
                    self._Xi = self.getIndividualExplorationRegion()
                
                self._searchState = SearchStates.OUTREGION
                self.reinitZigzagParams()
                self._y0 = self._xi.copy()
                self._Deltay = 0
                self._AB = set([])
        
    def update(self, meetings):
        
        if not self.isEnded():
            
            if self._robotState in [RobotStates.SEARCHING,  RobotStates.WAITING]:
                
                Ii, Wi = self.newInteraction(meetings) 
                if len(Ii)>=1:
                    self._Ii, self._Wi = Ii.copy(), Wi.copy()
                    self._robotState = RobotStates.STOP
                    self._tw = self._t
                    
                elif len(Wi)>=1:
                    
                    self._robotState = RobotStates.WAITING
                else:
                    self.search()
                    
                         
            elif self._robotState == RobotStates.STOP:
                if (self._t-self._tw)<=self._Tw:
                    self.waiting(meetings)
                    
                else:
                    self._robotState = RobotStates.PLANNING
                                    
            elif self._robotState == RobotStates.PLANNING:
                if self._planningState == PlanningStates.NONE:
                    self._pruningObject = PruningNode(self._id, self._Ii)
                    self._pruningObject.initialOneHop()
                    self._planningState = PlanningStates.CONSTRUCTION
                    self._stepB = "update"
                    self._GO = False
                    
                elif self._planningState==PlanningStates.CONSTRUCTION:
                    self.treeConstruction()
                    
                elif self._planningState==PlanningStates.ELECTION:
                    
                    self.leaderElection()
                    
                elif self._planningState==PlanningStates.FUSION:
                    self.dataFusion()
                
                elif self._planningState == PlanningStates.TASK:
                    self.taskAssignment()
        
        ball = Ball(self._xi, self._d)
        self._Cit.append(self._Cit[-1].union(ball.Ball))
        self._Bit.append(self._Bit[-1].union(ball.Ball))
        self._t +=self._deltaT
        self._DeltaTi +=self._deltaT
        
    def treeConstruction(self):
        
        if not self._pruningObject.isEnded():
            if self._stepB == "update":
                self._pruningObject.initialUpdate()
                self._pruningObject.firstPruningDetection()
                self._stepB = "nexthop"
            elif self._stepB == "nexthop":
                self._pruningObject.nextOneHop()
                self._stepB = "nextupdate"
            else:
                self._pruningObject.nextUpdate()
                self._stepB = "nexthop"
        else:
            self._planningState = PlanningStates.ELECTION
            self._leaderObject = LeaderElection(self._pruningObject)
            self._leaderObject.firstHop()
            self._GO = True
        
    def leaderElection(self):

        if self._GO:
            self._leaderObject.treeParentUpdate()
            self._GO = False
        else:
            self._leaderObject.treeChildrenUpdate()
            self._GO = True
        if self._leaderObject.isEnded():
            BFS_GO = self._messaging.deserialize(self._msgType.BFS_GO)
            BFS_BACK = self._messaging.deserialize(self._msgType.BFS_BACK)
            endGO = self._id not in BFS_GO  or  (self._id in BFS_GO and len(BFS_GO[self._id])==0)
            endBACK =  self._id not in BFS_BACK  or  (self._id in BFS_BACK and len(BFS_BACK[self._id])==0)
            if endGO and endBACK:
                self._treeChildren = self._leaderObject._treeChildren
                self._treeParent = self._leaderObject._treeParent
                self._planningState=PlanningStates.FUSION
    
    def getCentralPoint(self):
        
        P = len(self._Li)
        c = np.array([0, 0])
        for j in self._Li:
            c = c + self._Li[j]
        return c/P
    
    def timeOfPreviousMeeting(self, j):
        
        tm = -1
        if j in self._Gi:
            tm = self._Gi[j]
        return tm
    
    def getCostMatrix(self, B):
        
        cost = []
        ids  = []
        for i in self._Li:
            ids.append(i)
            row = []
            for a in B:
                row.append(np.linalg.norm(np.array(self._Li[i])-a))
            cost.append(row)
        return ids, cost
        
    def newInteraction(self, meetings):

        withPerceptionRange = meetings
        Ii = set([])
        Wi = set([])
        for (j, state, tw) in withPerceptionRange:
            tm = self.timeOfPreviousMeeting(j)
            if (tm==-1 or self._DeltaTi>self._Ti) and (state==RobotStates.SEARCHING or state == RobotStates.STOP):
                Ii.add(j)
                self._roboState = RobotStates.STOP
            elif state == RobotStates.PLANNING and j not in self._Ii:
                Wi.add(j)
                self._robotState = RobotStates.STOP
                
        return Ii, Wi
    
    def relayInformation(self):
        
        F = self._treeChildren.copy()
        TASK = self._messaging.deserialize(self._msgType.TASK)
        while F!=set([]):
            j = F.pop()
            Gamma = {}
            
            for k in self._Dij[j]:
                Gamma[k] = self._Gamma[k]
            
            TASK = self._messaging.addTo(TASK, j, self._id, [self._Bit[-1], self._Zi, self._Vi, self._Gi, self._Jit[-1], Gamma])
            
        self._messaging.serialize(TASK, self._msgType.TASK)
        
            
    def taskAssignment(self):
        
        if self._treeParent == None:
            self.getGroupExplorationRegions()
            for j in self._treeChildren:
                for k in self._Gamma:
                    self._Gi[k] = self._t
                    Qk = self.rectangleToSet(self._Gamma[k])
                    self._Bit.append(self._Bit[-1].union(Qk))
                    
            self.relayInformation()
            
            self._y0 = self._xi.copy()
            self._Deltay = 0 
                
            self._Yit.append(self._Gamma[self._id])
            self.reinitInteractionInfo()
                    
        else:
            TASK = self._messaging.deserialize(self._msgType.TASK)
            if self._id in TASK:
                
                if(len(TASK[self._id])==1):
                    
                    j = list(TASK[self._id].keys())[0]
                    [Bj, Zj, Vj, Gj, Jj, Gammaj]= TASK[self._id][j]
                    self._Bit.append(self._Bit[-1].union(Bj))
                    self._Zi = self._Zi.union(Zj)
                    Qi = self.rectangleToSet(self._Vi).union(self.rectangleToSet(Vj))
                    self._Vi = self.setToRectangle(Qi)
                    self._Gi.update(Gj)
                    if len(self._Jit)==0:
                        self._Jit.append(Jj)
                    else:
                        self._Jit[-1] = self._Jit[-1].union(Jj)
                    self._Gamma.update(Gammaj)
                        
                    TASK = self._messaging.deleteFrom(TASK, self._id, j)
 
                self._messaging.serialize(TASK, self._msgType.TASK)
                self.relayInformation()

                self._robotState = RobotStates.SEARCHING
                if self._id in self._Gamma:
                    self._y0 = self._xi
                    self._Deltay = 0 
                    self._Yit.append(self._Gamma[self._id])
                self.reinitInteractionInfo()
                
    def dataFusion(self):
        
        F = self._treeChildren.copy()
        self._Li[self._id] = self._xi.copy()
        self._Di = set([])
        self._Dij = {}
        
        if len(F)==0:
            DATA = self._messaging.deserialize(self._msgType.DATA)
            j = self._treeParent
            DATA = self._messaging.addTo(DATA, j, self._id, [self._Cit[-1].copy(), self._Zi.copy(), self._Ii.union(set([self._id])), self._Li.copy(), self._Di.copy()])
            
            self._messaging.serialize(DATA, self._msgType.DATA)
            self._planningState = PlanningStates.TASK
            
        else:
            DATA = self._messaging.deserialize(self._msgType.DATA)
            if self._id in DATA:
                self._sendingInformation = True
                if len(F)==len(DATA[self._id]):
                    while(len(DATA[self._id])>=1):
                        j = list(DATA[self._id].keys())[0]
                        self._Di.add(j)
                        [Cj, Zj, Ij, Lj, Dj]= DATA[self._id][j]
                        self._Cit[-1] = self._Cit[-1].union(Cj)
                        self._Ii = self._Ii.union(Ij)
                        self._Li.update(Lj)
                        if len(self._Jit)==0:
                            self._Jit.append(self._Ii)
                        else:
                            self._Jit.append(self._Jit[-1].union(self._Ii))
                        if j in self._Dij:
                            self._Dij[j] = self._Dij[j].union(Dj)
                        else:
                            self._Dij[j] = Dj.copy()
                        self._Di = self._Di.union(Dj)
                        self._Zi = self._Zi.union(Zj)
                        
                        self._Dij[j].add(j)
                            
                        self._messaging.deleteFrom(DATA, self._id, j)
                        
                    self._messaging.serialize(DATA, self._msgType.DATA)
                    
                    DATA = self._messaging.deserialize(self._msgType.DATA)
                    for j in F:
                        DATA = self._messaging.addTo(DATA, j, self._id, [self._Cit[-1].copy(), self._Zi.copy(), self._Ii.union(set([self._id])), self._Li, self._Di])
                        
                    if self._treeParent==None:
                        self._messaging.serialize(DATA, self._msgType.DATA)
                        self._planningState = PlanningStates.TASK
           
    def waiting(self, meetings):
        
        withPerceptionRange = meetings
        for (j, state, tw) in withPerceptionRange:
            if (state == RobotStates.SEARCHING or state == RobotStates.STOP) and j not in self._Ii:
                self._tw = max(tw, self._tw)
                self._Ii.add(j)
        
    def getClosestCorner(self, Xi, xi):
        
        corners = self.getCorners(Xi)
        c = corners.pop()
        u = np.linalg.norm(np.array(c)-xi)
        for ci in corners:   
            ui = np.linalg.norm(np.array(ci)-xi)
            if ui<u:
                c = ci
                u = ui
        return c
    
    def getCorners(self, Xi):
        
        x, y, w, h = int(Xi.x), int(Xi.y), int(Xi.w), int(Xi.h)
        return set([(x, y),(x, y + h), (x + w, y), (x + w, y + h)])
    
    def getGroupExplorationRegions(self, Mi=100):

        xmin, ymin = min(self._Bit[-1])
        xmax, ymax = max(self._Bit[-1])
        self._Vi = Rectangle([xmin, ymin], xmax-xmin, ymax-ymin)
        dtau = self._tau - self._t
        A = (np.pi*self._d**2+2*self._d*self._gamma*(dtau))
        A = (np.sqrt(A)+2*self._m)**2
        xi = self.getCentralPoint()
        self.virtualWorldEnlargement()
        P = len(self._Ii)
        k = 1
        Xs = []
        Ms = []
        self._Bit[-1] = self._Bit[-1].union(self._Cit[-1])
        U = self._Bit[-1].copy()
        B = []
        
        while k<=P:
            l = 1
            nextSampling = True
            while nextSampling:
                
                VS = self.rectangleToSet(self._Vi)
                Q = VS.difference(U)
                Qr = list(Q)
                N = min(Mi, len(Qr))
                indices = np.random.choice([j for j in range(len(Qr))], replace=False, size=N)
            
                for i in indices:
                    
                    w = int(np.random.randint(2*(self._d+self._m), np.sqrt(A)+ 2*(self._d+self._m)))
                    h = max(2*(self._d+self._m), int(A/w))
                    lx, ly = Qr[i]
                    R = Rectangle([lx, ly], w, h)
                    RS = self.rectangleToSet(R)
                    
                    if RS.intersection(U)==set([]) and RS.issubset(VS):
                        
                        U.add((lx, ly))
                        U = U.union(RS)
                        X = Rectangle([R.x+self._m, R.y+self._m], R.w-2*self._m, R.h-2*self._m)
                        XSet = self.rectangleToSet(X)
                        M = RS.difference(XSet)
                        Xs.append(X)
                        Ms.append(M)
                        B.append(self.getClosestCorner(X, xi))
                        k +=1
                        nextSampling = False
                        break
                
                l +=1
                if l==self._L:
                    l = 1
                    self.virtualWorldEnlargement()
                    
        robotsIDS, cost = self.getCostMatrix(B)
        ids, targets = hungarian(cost)
        self._Gamma = {robotsIDS[i]:Xs[a] for (i, a) in zip(ids, targets)}       
    
            
    def virtualWorldEnlargement(self):
        
        self._Vi.x -= self._w0
        self._Vi.y -= self._w0
        self._Vi.w +=2*self._w0
        self._Vi.h +=2*self._w0
        
    def getIndividualExplorationRegion(self, M=100):

        dtau = self._tau - self._t
        A = (np.pi*self._d**2+2*self._d*self._gamma*(dtau))/2
        l = 1
        U = self._Bit[-1].copy()
        while True:
            
            VS = self.rectangleToSet(self._Vi)
            Q = VS.difference(U)
            Qr = list(Q)
            N = min(M, len(Qr))
            indices = np.random.choice([j for j in range(len(Qr))], replace=False, size=N)
            w = int(np.random.randint(2*self._d, np.sqrt(A)+ 2*self._d))
            h = max(2*self._d,int(A/w))
            for i in indices:
                lx, ly = Qr[i]
                R = Rectangle([lx, ly], w, h)
                RS = self.rectangleToSet(R)
                if RS.intersection(U)==set([]) and RS.issubset(VS):
                    return R
                
            l +=1
            if l==self._L:
                l = 1
                self.virtualWorldEnlargement()

    def rectangleToSet(self, R):
        
        [x, y], width, height = [int(R.x), int(R.y)], int(R.w), int(R.h)
        return set([(i, j) for i in range(x, x+width) for j in range(y, y+height)])
    
    def rectangleExtraction(self, N, D):
        
        S = set([])
        L = D.copy()
        for j in range(N):
            if len(L)>=1:
                point = L.pop()
                S.add(point)
            else:
                break
            
        (xmin, ymin) = np.min(list(S), axis=0)
        (xmax, ymax) = np.max(list(S), axis=0)
        R = set([])
        for (l, h) in S:
            L = set([(px, py) for (px, py) in S if (xmin<=l<=xmax) and (h==py)])
            H = set([(px, py) for (px, py) in S if (ymin<=h<=ymax) and (l==px)])
            
            if len(L.union(H))>=(ymax-ymin+xmax-xmin)/2:
                R = R.union(L).union(H)
        try:
            (xmin, ymin) = np.min(list(R), axis=0)
            (xmax, ymax) = np.max(list(R), axis=0)
            return Rectangle([xmin, ymin], xmax-xmin, ymax-ymin)
        except:
            return Rectangle([0, 0], 0, 0)
    
    def setToRectangle(self, S):
            
        (xmin, ymin) = np.min(list(S), axis =0)
        (xmax, ymax) = np.max(list(S), axis = 0)
        return Rectangle([xmin, ymin], xmax-xmin, ymax-ymin)
    
    
    def finExplorationAreaInRegion(self):
        
        xmin, xmax = int(self._Xi.x), int(self._Xi.x+self._Xi.w)
        ymin, ymax = int(self._Xi.y), int(self._Xi.y+self._Xi.h)
        X = set([(px, py) for px in range(xmin, xmax) for py in range(ymin, ymax)])
        Q = X.difference(self._Bit[-1])
        A = np.pi*self._d**2 + 2*self._d*self._gamma*(self._tau-self._t)/2
            
        xmin, xmax, ymin, ymax = int(self._Vi.x), int(self._Vi.x+self._Vi.w), int(self._Vi.y), int(self._Vi.y+self._Vi.h)
        V = set([(px, py) for px in range(xmin, xmax) for py in range(ymin, ymax)])
        Q = self.rectangleExtraction(self._N, V.difference(self._Cit[-1]))
        
        if Q.w*Q.h>=A:
            
            self._Xi = Q
            
        elif len(self._Yit)>0:
            self._Xi = self._Yit.pop()
        else:
            self._Xi = self.getIndividualExplorationRegion()
            self._Yit = []
    
    
                
    

            
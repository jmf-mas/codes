from coordinationUtils import Environment, Meeting
import os
from ars import ARS
import numpy as np
from messageTypes import MessageTypes
from robotStates import RobotStates

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QDesktopWidget, QFrame
from PyQt5.QtGui import QPainter, QColor, QPen, QFont
from PyQt5.QtWidgets import QPushButton, QProgressBar
from PyQt5.QtCore import Qt, QTimer

m = MessageTypes
if not os.path.exists("out"):
    os.mkdir("out")
for msg in m:
    if os.path.exists(msg.value):
        os.remove(msg.value)  
    os.mknod(msg.value)

width, height, w, h, N = 600, 600, 60, 40, 1

tau = 200
d = 10
dt = 1
C = set([])
environment = Environment(d, width, height, w, h, N)

robotObjects = [ARS(i, tau, d, environment.getEnv(i)) for i in range(N)]
robotPaths = {i:[] for i in range(N)}
robotPerformances = {i:[] for i in range(N)}

interaction  = Meeting()
O, _ = environment.getEnv(0)


# colors
colors = [[QColor(np.random.uniform(0, 255), np.random.uniform(0, 255), np.random.uniform(0,255), 255), 
                   QColor(np.random.uniform(50, 170), np.random.uniform(20, 170), np.random.uniform(20, 170), 200), 
                   QColor(np.random.uniform(200, 255), np.random.uniform(200, 255), np.random.uniform(200, 255), 200)] for i in range(N)]


class App(QMainWindow):
 
    def __init__(self):
        super().__init__()
        self.title = 'Coordination of solitary robots'
        self.setWindowTitle(self.title)
        self.resize(width, height+30)
        # center the window
        qtRectangle = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.move(qtRectangle.topLeft())
        
        # Set window background color
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(p)
        
        # Add a frame for exploration
        self.exploration_frame = QFrame(self)
        self.exploration_frame.setFrameShape(QFrame.Box)
        self.exploration_frame.setFrameShadow(QFrame.Sunken)
        self.exploration_frame.setGeometry(0, 0, width, height)
        #self.setGeometry(300,300,1280,800)
        self.exploration_frame.setStyleSheet("background-color: rgb(255,255,255)")
        
        # font for labels
        font = QFont()
        font.setPointSize(11)
        font.setBold(True)
        # label for exploration world
        self.labelExploration = QLabel(self)
        self.labelExploration.setGeometry(15, 0, 150, 15)
        self.labelExploration.setFont(font)
        
        # start button
        self.start_button = QPushButton(self)
        self.start_button.setText("Start")
        self.start_button.setGeometry(0, height, 80, 30)
        self.start_button.clicked.connect(lambda:self.start())
        
        # game duration counter
        self.progress_game = QProgressBar(self)
        self.progress_game.setGeometry(width-100, height, 100, 30)
        self.progress_game.setValue(0)
        self.progress_game.setMaximum(100)
        
        self.obstacle_label = QLabel(self)
        self.obstacle_label.setText("- Black objects are obstacles")
        self.obstacle_label.setGeometry(100, height-5, 400, 25)
        
        self.meeting_label = QLabel(self)
        self.meeting_label.setText("- Temporary red circles indicate robot within perception range")
        self.meeting_label.setGeometry(100, height+10, 400, 25)
        
        # Initial painter
        self.exploration = PaintExploration(self.exploration_frame)
        self.exploration.move(0,0)
        self.exploration.resize(self.exploration_frame.size())
        self.exploration.update()
        self.exploration_frame.update()
        
        self.update()

        self.show()
        
    def start(self):
        
        global N, robotObjects, dt, robotObjects, robotPaths, robotPerformances, C
        
        
        self.timer_motion = [QTimer() for i in range(N)]
        
        for i in range(N):
            
            robotID =  robotObjects[i]._id
            robotPaths[robotID].append(robotObjects[robotID]._xi.tolist())
            Ci = robotObjects[robotID]._Cit[-1]
            Ci = Ci.difference(C).difference(robotObjects[i]._O)
            C = C.union(Ci)
            robotPerformances[robotID].append(len(Ci))
            
            self.timer_motion[robotID].timeout.connect(lambda robotID=robotID: self._update(robotID))
            self.timer_motion[robotID].start(dt)
    
    def _update(self, robotID):
        
        global N, robotObjects, robotPaths, robotPerformances, C
        
        if not robotObjects[robotID].isEnded():
        
            meetings = interaction.getMeetings(robotID, robotObjects, d)
            robotObjects[robotID].update(meetings)
            
            robotPaths[robotID].append(robotObjects[robotID]._xi.tolist())
            Ci = robotObjects[robotID]._Cit[-1]
            Ci = Ci.difference(C).difference(robotObjects[robotID]._O)
            C = C.union(Ci)
            perf = robotPerformances[robotID][-1] + len(Ci)
            robotPerformances[robotID].append(perf)
            if robotID==0:
                counter = int(100*robotObjects[robotID]._t/robotObjects[robotID]._tau)
                if counter < 100:
                    self.progress_game.setValue(counter)
    
            self.exploration = PaintExploration(self.exploration_frame)
            self.exploration.move(0,0)
            self.exploration.resize(self.exploration_frame.size())
            self.exploration.update()
            self.exploration_frame.update()
            self.show()
            
        else:
            self.progress_game.setValue(100)
            self.timer_motion[robotID].stop()
            print("robot = ", robotID, "coverage = ",  robotPerformances[robotID][-1])
      
class PaintExploration(QWidget):
    def paintEvent(exploration_space, event):
        qp_exploration = QPainter(exploration_space)
        qp_exploration.setPen(Qt.yellow)
        # Change origin point of QPainter
        qp_exploration.translate(exploration_space.rect().bottomLeft())
        qp_exploration.scale(1.0, -1.0)
        
        for i in range(N):
            if robotObjects[i]._robotState == RobotStates.PLANNING and robotObjects[i]._treeChildren!=set([]) and robotObjects[i]._treeParent == None:
                qp_exploration.setBrush(QColor(255, 0, 0, 127))
                x, y = robotObjects[i]._xi
                dr = 6*robotObjects[i]._d
                qp_exploration.drawEllipse(x-dr/2, y-dr/2, dr, dr)
                
         
        for i in range(N):
            qp_exploration.setPen(QPen(colors[i][1], 1, Qt.SolidLine))
            qp_exploration.setBrush(colors[i][0])
            
            qp_exploration.setPen(QPen(colors[i][0], 1, Qt.SolidLine))
            qp_exploration.setBrush(Qt.transparent)
        #   
            x, y = robotObjects[i]._xi
            qp_exploration.drawEllipse(x-2, y-2, 4, 4)
            
            qp_exploration.setBrush(colors[i][2])
            qp_exploration.setPen(QPen(colors[i][2], 1, Qt.SolidLine))
            
            for point in robotObjects[i]._Cit[-1]:
                px, py = point
                qp_exploration.drawPoint(px, py)
        
        qp_exploration.setBrush(QColor(0, 0, 0, 255))
        qp_exploration.setPen(QPen(QColor(0, 0, 0, 255), 1, Qt.SolidLine))
        
        for point in O:
            px, py = point
            qp_exploration.drawPoint(px, py) 

# Launch        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

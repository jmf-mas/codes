from coordinationUtils import Ball
import numpy as np

class Bug:
    
    def __init__(self, d, gamma):
        self._d = d
        self._gamma = gamma
        
    def distanceBug(self, A, xi, a, C, O):
        
        x = xi + self._gamma * (a-xi)/(np.linalg.norm(np.array(a)-xi))
        ball = Ball(x, self._d)
        I = len(ball.Ball.difference(C))
        if (int(x[0]), int(x[1])) not in O and I>0:
            return x
        else:
            x0 = xi+np.array([0, self._gamma])
            x1 = xi+np.array([self._gamma, 0])
            x2 = xi+np.array([-self._gamma, 0])
            x3 = xi + np.array([0, -self._gamma])
            
            B = set([tuple(x0), tuple(x1), tuple(x2), tuple(x3)])
            B = B.difference(O)
            
            if B==set([]):
                return xi
            
            b = B.pop()
            dist = np.linalg.norm(np.array(b)-a)
            ball = Ball(b, self._d)
            I = len(ball.Ball.difference(C))
            u = self.utility(dist, I)
            for bi in B:
                dist = np.linalg.norm(np.array(bi)-a)
                ball = Ball(bi, self._d)
                I = len(ball.Ball.difference(C))
                
                if u<self.utility(dist, I):
                    b = np.array(bi).copy()
                    u = self.utility(dist, I)
            return np.array(b)
    
    def utility(self, dist, I):
        
        return (1 + I)/(1 + dist)
    
    
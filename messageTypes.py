import enum
class MessageTypes(enum.Enum):
    # graph construction
    M = "out/M.ser"
    P = "out/P.ser"
    # leader
    BFS_GO = "out/BFS_GO.ser"
    BFS_BACK = "out/BFS_BACK.ser"
    #coordination
    DATA = "out/DATA.ser"
    TASK = "out/TASK.ser"
    
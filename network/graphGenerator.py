import numpy as np
import networkx as nx

def mixer(i, components):
    othercomponents =[]
    for j in range(len(components)):
        if i!=j:
            othercomponents.extend(components[j])
    return othercomponents

def smallest_component(components):
    i=0
    for j in range(1,len(components)):
        if len(components[j])<len(components[i]):
            i = j
    return i

def graphgenerator(N, width, radius):
    nodes_position = np.random.uniform(0, width, (N, 2))
    G=nx.Graph()
    
    for i in range(N):
        G.add_node(i)
    
    end = nx.is_connected(G)
    while not end:
        for i in range(N-1):
            for j in range(i+1, N):
                if np.linalg.norm(nodes_position[i]- nodes_position[j])<=radius:
                    G.add_edge(i, j)

        components=[]
        for nodes in nx.connected_components(G):
            components.append(list(nodes))
        if len(components)==1:
            return G
        i=smallest_component(components)
        jnode, inode, dist=-1, -1, 10000000000
        others=mixer(i, components)
        icomponent = components[i]
        for node in icomponent:
            distances = np.linalg.norm(nodes_position[node]-nodes_position[others], axis=1)
            if np.min(distances)<dist:
                dist = np.min(distances)
                jnode=others[np.argmin(distances)]
                inode=node
        knode = G.number_of_nodes()
        G.add_node(knode)
        position = (nodes_position[inode]+nodes_position[jnode])/2
        nodes_position= np.append(nodes_position, [position.tolist()], axis=0)
        N +=1
        end = nx.is_connected(G)
    return G
from pympler import asizeof
import time
import networkx as nx
import os
os.chdir("..")
from messaging import Messaging
from messageTypes import MessageTypes

class PruningNode:
    
    def __init__(self, i, Ni, D=10):
        
        self._id = i
        self._Ni = Ni.copy()
        self._NitUp = Ni.copy()
        self._Sit = [set([self.edge(i, j) for j in self._Ni])]
        self._Sjt = {j:[] for j in self._Ni}
        self._Si = set([self.edge(i, j) for j in self._Ni])
        self._Qi = []
        self._centrality = -1
        
        self._D = D
        self._equilibrium = False
        self._t = 0
        self._finished = False
        
        self._nbMessages = 0  
        self._runningTime = time.time()
        self._memorySize = 0
        
        self._msgType = MessageTypes
        self._messaging = Messaging()
        
        self._hold = False
    
    def edge(self, i, j):
        
        return (min(i, j), max(i, j))
    
    def initialOneHop(self):
        
        self._t += 1
        for j in self._NitUp:
            M = self._messaging.deserialize(self._msgType.M)
            M = self._messaging.addTo(M, j, self._id, self._Sit[-1])
            self._messaging.serialize(M, self._msgType.M)
            self._nbMessages +=1
            
    def initialUpdate(self):
        
        if not self._finished:
            M = self._messaging.deserialize(self._msgType.M)
            
            if self._id in M and not self._equilibrium and self._t<self._D:
                S = set([])
                Q = {self._id:set(self._NitUp)}
                
                while(len(M[self._id])>=1):
                    self._nbMessages +=1
                    j = list(M[self._id].keys())[0]
                    St = M[self._id][j]
                    
                    if j in self._Sjt:
                        self._Sjt[j].append(St)
                    else:
                        self._Sjt[j] = [St]
                    S = S.union(St)
                    q = set([k for l in St for k in l])
                    if self._t==1:
                        q = q.difference(set([j]))
                    Q[j] = q
                    M = self._messaging.deleteFrom(M, self._id, j)
                    
                self._messaging.serialize(M, self._msgType.M)
                
                self._Qi.append(Q)
                S = S.difference(self._Si.copy())
                self._Sit.append(S)
                self._Si = self._Si.union(S)
                
                self._equilibrium = S==set([]) 
        
    def leavesDetection(self):
        
        Fit = set([])
        for j in set(self._NitUp).union(set([self._id])):
            if len(self._Qi)>0 and j in self._Qi[0] and len(self._Qi[0][j])==1:
                Fit.add(j)
            
        return Fit
    
    def triangleDetection(self, Fit):
        
        for j in set(self._NitUp).union(set([self._id])):
            if len(self._Qi)>0 and j in self._Qi[0] and len(self._Qi[0][j])==2:
                f, g = self._Qi[0][j]
                if g in self._Qi[0] and f in self._Qi[0][g]:
                    Fit.add(j)
                    
        return Fit
    
    def furtherPruningDetection(self):
        
        Fit = set([])
        St = self._Si.difference(self._Sit[-1])
        
        for j in self._NitUp:
            if len(self._Sjt[j])>0 and self._Sjt[j][-1].issubset(St):
                Fit.add(j)
                
        if len(self._NitUp)==1 and self._Sit[-1]!=set():
            Fit.add(self._id)
            self._equilibrium = True
            self._hold =True
            
        return Fit
    
    def firstPruningDetection(self):
        
        self._hold = False
        Fit = self.leavesDetection()
        Fit =self.triangleDetection(Fit)
        self._Fit = Fit
        self._Fi = Fit
        if self._id in self._Fi:
            self._equilibrium = True
            self._hold = True
        
    def nextOneHop(self):
        
        if not self._finished:
            self._t += 1
            self._NitUp = set(self._NitUp).difference(self._Fi)
            if not self._equilibrium:
                for j in self._NitUp:
                     M = self._messaging.deserialize(self._msgType.M)
                     M = self._messaging.addTo(M, j, self._id, self._Sit[-1])
                     self._messaging.serialize(M, self._msgType.M)
                     self._nbMessages +=1
            
    def nextUpdate(self):
        
        if not self._finished:
            M = self._messaging.deserialize(self._msgType.M)
            
            if self._id in M and not self._equilibrium and self._t<self._D:
                S = set([])
                while(len(M[self._id])>=1):
                    self._nbMessages +=1
                    j = list(M[self._id].keys())[0]
                    St = M[self._id][j]
                    self._Sjt[j].append(St)
                    S = S.union(St)
                    M = self._messaging.deleteFrom(M, self._id, j)
                    
                self._messaging.serialize(M, self._msgType.M)
    
                S = S.difference(self._Si.copy())
                self._Sit.append(S)
                self._Si = self._Si.union(S)
                
                self._Fit=self.furtherPruningDetection()
                self._Fi = self._Fi.union(self._Fit)
                
                self._equilibrium = S==set([])
    
    def isEnded(self):
        
        if not self._finished:
            self._finished = self._t==self._D or self._equilibrium or self._hold
            if self._finished:
                
                self._runningTime = time.time()- self._runningTime
                self._memorySize = 0.000001*asizeof.asizeof([self])
                if not self._hold and len(self._Si)>0:
                    G = nx.Graph()
                    G.add_edges_from(self._Si)
                    self._centrality = nx.closeness_centrality(G)[self._id]
                else:
                    self._centrality = 0
                
        return self._t==self._D or self._equilibrium or self._hold

        
            
      
 
        
    
    
    
                
    
                
        
            
        
        
        
            
    


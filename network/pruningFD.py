import time
import numpy as np
from pruning import PruningNode
from pympler import asizeof
              
class PruningNodeFD(PruningNode):
    
    def __init__(self, i, Ni, D=10, T=0.01, pf=0.1, pr=0.8):
        
        super(PruningNodeFD, self).__init__(i, Ni)
        
        self._Gi = set([])
        self._NiDown = set([])
        self._SiDown = set([])
        self._EiDown = set([])
        self._Ti = {j: time.time() for j in self._Ni}
        self._T = T
        self._pf = pf
        self._pr = pr
        self._isDown = False
    
    def isPersistent(self, signal):
        
        if signal not in self._Gi and np.random.choice([0, 1], p=[self._pf, 1-self._pf])==1:
            self._Gi  = self._Gi.union(signal)
            (S, Q, c) = signal
            if (1-S, Q, c) in self._Gi:
                self._Gi.remove((1-S, Q, c))
            return True
        return False
    
    def nodeFailureDetection(self, j):
        
        if time.time()-self._Ti[j]>self._T:
            self._NitUp.remove(j)
            self._NiDown.add(j)
            for k in self._NitUp:
                P = self._messaging.deserialize(self._msgType.P)
                P = self._messaging.addTo(P, k, self._id, [0, 0, j])
                self._messaging.serialize(P, self._msgType.P)

                
    def edgeFailureDetection(self, j):
        
        isDown = False
        if self.edge(self._id, j) not in self._Gi:
            if np.random.choice([0, 1], p=[self._pf, 1-self._pf])==0:
                self._Gi.add(self.edge(self._id, j))
                isDown = True
                    
            P = self._messaging.deserialize(self._msgType.P)
            if isDown:
                self._SiDown.add(self.edge(self._id, j))
                self._EiDown.add(j)
                for k in self._NitUp:
                    P = self._messaging.deserialize(self._msgType.P)
                    P = self._messaging.addTo(P, k, self._id, [0, 1, self.edge(self._id, j)])
                    self._messaging.serialize(P, self._msgType.P)
                self._nbMessages +=1
        return isDown
                
    def recoveryDetection(self, j, isDown):
        
        if isDown and j in self._NiDown:
            if np.random.choice([0, 1], p=[self._pr, 1-self._pr])==0:
                self._NitUp.add(j)
                self._NiDown.remove(j)
                self._EiDown.add(j)
                P = self._messaging.deserialize(self._msgType.P)
                for k in self._NitUp:
                    P = self._messaging.addTo(P, j, self._id, [0, 1, j])
                    self._messaging.serialize(P, self._msgType.P)
                self._nbMessages +=1
                    
        if isDown and self.edge(self._id, j) in self._SiDown:
            self._SiDown.remove(self.edge(self._id, j))
            self._EiDown.remove(j)
            P = self._messaging.deserialize(self._msgType.P)
            for k in self._NitUp:
                P = self._messaging.addTo(P, j, self._id, [1, 1, self.edge(self._id, j)])
                self._messaging.serialize(P, self._msgType.P)
            self._nbMessages +=1
            
        
    def forwardFailureSignals(self):
        
        P = self._messaging.deserialize(self._msgType.P)
        if self._id in P:
            
            while(len(P[self._id])>=1):
                j = list(P[self._id].keys())[0]
                (S, Q, c) = P[self._id][j]
                if self.isPersistent((S, Q, c)):
                    if (S, Q)==(0, 0):
                        self._NiDown.add(c)
                    if (S, Q)==(0, 1):
                        self._SiDown.add(c)
                    if (S, Q)==(1, 0):
                        self._NiDown = self._NiDown.difference(set([c]))
                    if (S, Q)==(1, 1):
                        self._SiDown = self._SiDown.difference(set([c]))
                    for k in self._NitUp:
                        P = self._messaging.addTo(P, k, self._id, [S, Q, c])
                    self._messaging.serialize(P, self._msgType.P)
                    
                self._messaging.deleteFrom(P, self._id, j)
                self._nbMessages +=1
    
    def initialOneHop(self):
        
        if not self._finished:
            self._t += 1
            
            if not self._isDown:
                if np.random.choice([0, 1], p=[self._pf, 1-self._pf])==0:
                    self._isDown = True
                else:
                    for j in self._NitUp:
                        M = self._messaging.deserialize(self._msgType.M)
                        M = self._messaging.addTo(M, j, self._id, self._Sit[-1])
                        self._messaging.serialize(M, self._msgType.M)
                        self._Ti[j] = time.time()
                        
                        if np.random.choice([0, 1], p=[self._pf, 1-self._pf])==0:
                            self._Gi.add(self.edge(self._id, j))
            else:
                if np.random.choice([0, 1], p=[self._pr, 1-self._pr])==0:
                    self._isDown = False
                    
    def nextOneHop(self):
        
        if not self._finished:
            self._t += 1
            self._NitUp = set(self._NitUp).difference(self._Fi)
            if not self._isDown:
                if np.random.choice([0, 1], p=[self._pf, 1-self._pf])==0:
                    self._isDown = True
                else:
                    if not self._equilibrium:
                        for j in self._NitUp:
                            M = self._messaging.deserialize(self._msgType.M)
                            M = self._messaging.addTo(M, j, self._id, self._Sit[-1])
                            self._messaging.serialize(M, self._msgType.M)
                            self._Ti[j] = time.time()
                            
                            if np.random.choice([0, 1], p=[self._pf, 1-self._pf])==0:
                                self._Gi.add(self.edge(self._id, j))
            else:
                if np.random.choice([0, 1], p=[self._pr, 1-self._pr])==0:
                    self._isDown = False
    
    def initialUpdate(self):
        
        if not self._finished:
            M = self._messaging.deserialize(self._msgType.M)
            if self._id in M:
                S = set([])
                Q = {self._id:set(self._NitUp)}
                
                while(len(M[self._id])>=1):
                    NitUp = self._NitUp.copy()
                    for k in NitUp:
                        self.nodeFailureDetection(k)
                    self._nbMessages +=1
                    j = list(M[self._id].keys())[0]
                    St = M[self._id][j]
                    self._Ti[j] = time.time()
                    isDown = self.edgeFailureDetection(j)
                    if not isDown:
                        self._Sjt[j].append(St)
                        S = S.union(St)
                        q = set([k for l in St for k in l])
                        if self._t==1:
                            q = q.difference(set([j]))
                        Q[j] = q
                    self.recoveryDetection(j, isDown)
    
                    Nup = self._NitUp.copy()
                    Nup = Nup.difference(self._EiDown)
                    Nup = Nup.difference(self._NiDown)
                    self._NitUp = Nup.copy()
                    M = self._messaging.deleteFrom(M, self._id, j)
                    
                self._messaging.serialize(M, self._msgType.M)
                
                self._Qi.append(Q)
                self.forwardFailureSignals()
                S = S.difference(self._Si.copy())
                self._Sit.append(S)
                self._Si = self._Si.union(S)
                
                self._equilibrium = S==set([]) 
                self.finalIteration()
            
    def nextUpdate(self):
        
        if not self._finished:
            M = self._messaging.deserialize(self._msgType.M)
            if self._id in M and not self._equilibrium:
                S = set([])
                while(len(M[self._id])>=1):
                    NitUp = self._NitUp.copy()
                    for k in NitUp:
                        self.nodeFailureDetection(k)
                    self._nbMessages +=1
                    j = list(M[self._id].keys())[0]
                    St = M[self._id][j]
                    self._Sjt[j].append(St)
                    S = S.union(St)
                    
                    self._Ti[j] = time.time()
                    isDown = self.edgeFailureDetection(j)
                    
                    if not isDown:
                        self._Sjt[j].append(St)
                        S = S.union(St)
                    
                    self.recoveryDetection(j, isDown)
                    
                    Nup = self._NitUp.copy()
                    Nup = Nup.difference(self._EiDown)
                    Nup = Nup.difference(self._NiDown)
                    self._NitUp = Nup.copy()
                    M = self._messaging.deleteFrom(M, self._id, j)
                    
                self._messaging.serialize(M, self._msgType.M)
    
    
                S = S.difference(self._Si.copy())
                self._Sit.append(S)
                self._Si = self._Si.union(S)
                self.forwardFailureSignals()
                self._Fit=self.furtherPruningDetection()
                self._Fi = self._Fi.union(self._Fit)
                self._equilibrium = S==set([])
                self.finalIteration() 
                
    def finalIteration(self):
        
        if self._equilibrium and self._t<self._D:
            if not self._isDown:
                if np.random.choice([0, 1], p=[self._pf, 1-self._pf])==0:
                    self._isDown = True
                else:
                    if self._equilibrium and self._t<self._D:
                        Nup = self._NitUp.copy()
                        Nup = Nup.difference(self._EiDown)
                        Nup = Nup.difference(self._NiDown)
                        self._NitUp = Nup.copy()
                        for j in self._NitUp:
                            M = self._messaging.deserialize(self._msgType.M)
                            M = self._messaging.addTo(M, j, self._id, self._Sit[-1])
                            self._messaging.serialize(M, self._msgType.M)
                                
                    if self._equilibrium:
                        E = set([])
                        for j in self._NiDown:
                            for k in self._NiDown:
                                if self._id!=k:
                                    E.add(self.edge(j, k))
                        self._Si = self._Si.difference(E)
            else:
                if np.random.choice([0, 1], p=[self._pr, 1-self._pr])==0:
                    self._isDown = False
    
    def isEnded(self):
        
        if not self._finished:
            self._finished = self._t==self._D or self._equilibrium
            if self._finished:
                self._runningTime = time.time()- self._runningTime
                self._memorySize = 0.000001*asizeof.asizeof([self])
                
        return self._t==self._D or self._equilibrium

        
    
    
        
    
    
 
        
    
    
    
                
    
                
        
            
        
        
        
            
    


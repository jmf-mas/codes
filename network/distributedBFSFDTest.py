import os
from distributedBFSFD import DistributedBFSFD
os.chdir("network")
from pruningFD import PruningNodeFD  
os.chdir("network")
from graphGenerator import graphgenerator as graph
from messageTypes import MessageTypes
import os

m = MessageTypes
if not os.path.exists("out"):
    os.mkdir("out")
for msg in m:
    if os.path.exists(msg.value):
        os.remove(msg.value)  
    os.mknod(msg.value)

def running_you(G, D=10):
    N= G.number_of_nodes()
    yobjects = [YouNodeFD(i, set(G.neighbors(i))) for i in G.nodes()]
    isEnd = False
    while not isEnd:
        for i in range(N):
            yobjects[i].oneHop()
            
        for i in range(N):
            yobjects[i].update()
        end = True
        for i in range(N):
            end = end and yobjects[i].isEnded() 
        isEnd = end

    total_msg = 0
    max_msg = 0
    max_time = 0
    min_memory = 99999999999999999999999999999999
    max_memory = 0
    for i in range(N):
        _msg, _time, _memory = yobjects[i]._nbMessages, yobjects[i]._runningTime, yobjects[i]._memorySize
        max_time = max(max_time, _time)
        min_memory = min(min_memory, _memory)
        max_memory = max(max_memory, _memory)
        max_msg = max(max_msg, _msg)
        total_msg +=_msg   
    return max_time, total_msg, max_msg, min_memory, max_memory

def running_pruning(G, D=10):
    N= G.number_of_nodes()
    pobjects = [PruningNodeFD(i, set(G.neighbors(i))) for i in G.nodes()]
    
    isEnd = False
    for i in range(N):
        pobjects[i].initialOneHop()
    for i in range(N):
        pobjects[i].initialUpdate()
    
    for i in range(N):
        pobjects[i].firstPruningDetection()
    
    while not isEnd:
    
        end = True
        for i in range(N):
            pobjects[i].nextOneHop()
        for i in range(N):
            pobjects[i].nextUpdate()
        for i in range(N):
            end = end and pobjects[i].isEnded() 
        isEnd = end

    total_msg = 0
    max_msg = 0
    max_time = 0
    min_memory = 99999999999999999999999999999999
    max_memory = 0
    for i in range(N):
        (_msg, _time, _memory) = pobjects[i]._nbMessages, pobjects[i]._runningTime, pobjects[i]._memorySize
        max_time = max(max_time, _time)
        min_memory = min(min_memory, _memory)
        max_memory = max(max_memory, _memory)
        max_msg = max(max_msg, _msg)
        total_msg +=_msg   
    return max_time, total_msg, max_msg, min_memory, max_memory

N = 60
radius = 8
width = 40
print("Graph creation...")
G = graph(N, width, radius)
print("Graph creation done")
D = 10
print("Method by You")
print(running_you(G, D))
print("Pruning method")
print(running_pruning(G, D))

import numpy as np
from scipy.stats import ttest_ind
from scipy.stats import wilcoxon

def welch_t_test(xdata, ydata):
    return ttest_ind(xdata, ydata)[1]

def wilcoxon_test(xdata, ydata):
    return wilcoxon(xdata, ydata)[1]
    
def effect_size(xdata, ydata):
    x_mean, x_std = np.mean(xdata), np.std(xdata)
    y_mean, y_std = np.mean(ydata), np.std(ydata)
    N = len(xdata)
    return (x_mean-y_mean)/np.sqrt((N-1)*(x_std**2+y_std**2)/(2*N))

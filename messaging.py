import os
import pickle

class Messaging:  
    
    def serialize(self, msgContent, msgType):
        
        outfile = open(msgType.value, 'wb')
        pickle.dump(msgContent, outfile)
        outfile.close()
        
    def deserialize(self, msgType):
        
        if os.path.getsize(msgType.value)>0:
            infile = open(msgType.value, 'rb')
            msgContent = pickle.load(infile)
            infile.close()
            return msgContent
        else:
            return {}
        
    def deleteFrom(self, items, i, j):  
        
        if i in items:
            if j in items[i]:
                del items[i][j]   
        return items.copy()
    
    def addTo(self, items, i, j, msg):
        
        if i in items:
            element = items[i]
            element[j] = msg
            items[i] = element
        else:
            items[i]={j:msg}
        return items.copy()


    
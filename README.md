## Network

- Benchmark method for failure-free cases: *distributedBFS.py*
- Pruning method for failure cases: *pruning.py*
- Benchmark method for failure cases: *distributedBFSFD.py*
- Pruning method for failure cases: *pruningFD.py*
- Tests: *distributedBFSTest.py and distributedBFSFDTest.py*
- Graph generator: *graphGenerator.py*
- Messaging: *messaging.py*

## Leader

- Leader election: *distributedBFSLD.py*
- Test: *distributedBFSLDTest.py*

## Coordination

- ARS: *ars.py*
- PRS/Hybrid; *prs.py*
- SOS: *sos.py*
- Zigzag search: zigzag.py
- Obstacle avoidance: bug.py
- Enum for robot states: robotStates.py
- Enum for planning states: planningStates.py
- Enum for search states: searchStates.py
- Util: *coordinationUtils.py*
- Tests: *sosTest.py, arsTest.py, prsTest.py*

## Classifier fusion

- all benchmark methods: *classifierFusion.py*
- yayambo: yayambo.py
- test: *classifierFusionTest.py*